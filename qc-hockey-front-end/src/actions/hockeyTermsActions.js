import { 
  GET_HOCKEY_TERMS, 
  SET_CATEGORY,
  GO_TO_NEXT_CARD,
  GO_TO_PREVIOUS_CARD
} from "./types";
import axios from 'axios';

export const getHockeyTerms = () => async dispatch => {
  try {
    const res = await axios.get('https://d3-qc-json-server.herokuapp.com/terms')

    dispatch({
      type: GET_HOCKEY_TERMS,
      payload: res.data
    })
    
  } catch(err) {
    console.log(err)
  }
}

export const setCategory = (cat) => {
  return {
    type: SET_CATEGORY,
    payload: cat
  }
}

export const goToNextCard = () => {
  return {
    type: GO_TO_NEXT_CARD
  }
}

export const goToPreviousCard = () => {
  return {
    type: GO_TO_PREVIOUS_CARD
  }
}