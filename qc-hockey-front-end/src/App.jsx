import Header from './components/Header';
import CardStudy from './components/CardStudy';
import { Provider } from 'react-redux'
import store from './store';

function App() {
  return (
    <Provider store={store}>
      <Header />
      <CardStudy/>
    </Provider>
  );
}

export default App;