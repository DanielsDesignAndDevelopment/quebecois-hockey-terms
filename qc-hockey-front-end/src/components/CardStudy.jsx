import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { 
  getHockeyTerms, 
  goToNextCard, 
  goToPreviousCard,
  setCategory 
} from '../actions/hockeyTermsActions';
import Card from './Card';
import CardNav from './CardNav';
import CardOptions from './CardOptions';
import NavLinks from './NavLinks';

function CardStudy () {
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(getHockeyTerms());
  }, [dispatch])

  const hockeyTermsRoot = useSelector(state => state.hockeyTermsRoot);
  const { hockeyTerms, cardCategory, cardCounter } = hockeyTermsRoot;

  const cardContent = hockeyTerms.length && hockeyTerms.filter(term => term.cat === cardCategory);
  const frontOfCard = cardContent ? cardContent[cardCounter].qc : "Loading...";
  const backOfCard = cardContent ? cardContent[cardCounter].en : "Loading...";

  return (
    <div className="container text-center my-5">
      <p style={blueStyle} className="mt-3">Click card to flip</p>
      <Card frontContent={frontOfCard} backContent={backOfCard}/>
      <CardNav 
        onClickNext={() => dispatch(goToNextCard())} 
        onClickPrevious={() => dispatch(goToPreviousCard())}
      />
      <CardOptions 
        style={centerStyle}
        terms={hockeyTerms} 
        category={cardCategory} 
        onChange={(e) => dispatch(setCategory(e.target.value))}
      />
      <NavLinks linkStyle={blueStyle} />
    </div>
  )
}

const centerStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center"
}

const blueStyle = {
  color: "#003399",
  textDecoration: "none"
}

export default CardStudy;