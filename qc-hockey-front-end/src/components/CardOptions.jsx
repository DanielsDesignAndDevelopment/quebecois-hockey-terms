import { Form } from 'react-bootstrap';
import PropTypes from 'prop-types';
function CardOptions ({ terms, cardCategory, onChange, style }) {

  /* The following function takes the "cat" key value/pair out the 
  object that is fed into it. It only takes each category once then 
  the categories are mapped as options to the user in the select form. */

  const isolateTermCategories = (rawTerms) => {
    const isolatedTermCats = [];

    rawTerms.forEach(rawTerm => !isolatedTermCats.includes(rawTerm.cat)
      && isolatedTermCats.push(rawTerm.cat));

      return isolatedTermCats;    
  }
  
  const categories = isolateTermCategories(terms);

  return (
    <Form style={style}>
      <Form.Select
        value={cardCategory}
        name="category"
        onChange={onChange}
        className="text-center mb-3"
        style={formStyle}
      >
        {categories.map(category => 
          <option key={category} value={category}>{category}</option>)}
      </Form.Select>
    </Form>
  )
}

const formStyle = {
  width: "320px",
  color: "#003399"
}

CardOptions.propTypes = {
  terms: PropTypes.array,
  cardCategory: PropTypes.string,
  onChange: PropTypes.func,
  style: PropTypes.object
}

export default CardOptions;