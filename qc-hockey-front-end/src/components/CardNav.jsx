import { GoArrowLeft, GoArrowRight } from 'react-icons/go';
import PropTypes from 'prop-types';

function CardNav ({ onClickNext, onClickPrevious }) {
  return (
    <div className="mb-2">
      <button onClick={onClickPrevious} style={buttonStyle}>
        <GoArrowLeft />Prev
      </button>
      <button onClick={onClickNext} style={buttonStyle}>
        Next<GoArrowRight />
      </button>
    </div>
  )
}

const buttonStyle = {
  color: "white",
  backgroundColor: "#003399",
  border: 0,
  borderRadius: "5px",
  padding: "5px",
  margin: "2px",
  width: "157px"
}

CardNav.propTypes = {
  onClickNext: PropTypes.func,
  onClickPrevious: PropTypes.func,
}

export default CardNav;