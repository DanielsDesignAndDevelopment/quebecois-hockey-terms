import quebecFlag from '../images/quebecFlag.png';

function Header () {
  return (
    <header className="text-white" style={headerStyle}>
      <img src={quebecFlag} alt="Quebec Flag" style={flagStyle} />
      <h1 className="mx-3">Québécois Hockey Terms</h1>
    </header>
  )
}

const headerStyle = {
  height: "60px",
  display: "flex",
  justifyContent: "flex-start",
  alignItems: "center",
  borderBottom: "1px solid white",
  backgroundColor: "#003399",
  fontStyle: "italic"
}

const flagStyle = {
  width: "50px",
  height: "auto",
  marginLeft: "20px"
}

export default Header;