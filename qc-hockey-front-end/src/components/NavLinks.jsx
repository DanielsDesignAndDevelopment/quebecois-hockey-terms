import { FaBitbucket } from 'react-icons/fa';

function NavLinks () {
  return (
    <div>
      <a 
        href="https://bitbucket.org/DanielsDesignAndDevelopment/quebecois-hockey-terms"
        style={linkStyle}       
      >
        <FaBitbucket style={iconStyle} /> See code
      </a>
      <span style={iconStyle}>|</span>
      <a href="https://danielsdnd.com" style={linkStyle}>
        Back to danielsdnd.com
      </a>
    </div>
  )
}

const linkStyle = {
  color: "#003399",
  textDecoration: "none",
  margin: "10px"
}

const iconStyle = {
  color: "#003399",
  position: "relative",
  top: "-2px"
}

export default NavLinks;