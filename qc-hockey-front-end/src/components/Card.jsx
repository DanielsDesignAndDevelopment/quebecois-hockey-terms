import PropTypes from 'prop-types';

function Card ({ frontContent, backContent }) {

  const flipCard = (e) => {
    const element = e.currentTarget;
    element.style.transform === "rotateY(180deg)"
      ? element.style.transform = "rotateY(0deg)"
      : element.style.transform = "rotateY(180deg)"    
  }

  return (
    <div style={cardContainer}>
      <div style={theCard} onClick={e => flipCard(e)}>
        <div style={theFront}>
          <div style={content} id='frontContent'>
            {frontContent}
          </div>
        </div>
        <div style={theBack}>
          <div style={content} id='backContent'>
            {backContent}
          </div>
        </div>
      </div>
    </div>
  )
}

const cardContainer = {
  position: "relative",
  display: "inline-block",
  height: "250px",
  width: "320px",
}

const theCard = {
  height: "250px",
  width: "320px",
  transformStyle: "preserve-3d",
  transition: "all 0.5s ease",
  borderRadius: "15px",
  border: "1px solid white",
  backgroundColor: "white",
}

const theFront = {
  position: "absolute",
  width: "100%",
  height: "100%",
  backfaceVisibility: "hidden",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  transform: "rotateX(0deg)",
}

const theBack = {
  position: "absolute",
  width: "100%",
  height: "100%",
  backfaceVisibility: "hidden",
  transform: "rotateY(180deg)",
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
}

const content = {
  fontSize: "35px",
  fontWeight: "bold",
  fontStyle: "italic",
  color: "#003399",
  padding: "50px"
}

Card.propTypes = {
  frontContent: PropTypes.string,
  backContent: PropTypes.string
}

export default Card;