import { combineReducers } from 'redux';
import hockeyTermsReducer from './hockeyTermsReducer';

export default combineReducers({
  hockeyTermsRoot: hockeyTermsReducer
});