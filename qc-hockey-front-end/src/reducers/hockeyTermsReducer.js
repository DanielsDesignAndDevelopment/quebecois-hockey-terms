import { 
  GET_HOCKEY_TERMS,
  SET_CATEGORY,
  GO_TO_NEXT_CARD,
  GO_TO_PREVIOUS_CARD
} from "../actions/types";

const initialState = {
  hockeyTerms: [],
  cardCategory: "Equipment",
  cardCounter: 0
}

// eslint-disable-next-line
export default (state=initialState, action) => {
  switch(action.type) {
    case GET_HOCKEY_TERMS:
      return {
        ...state,
        hockeyTerms: action.payload
      }
    case SET_CATEGORY:
      return {
        ...state,
        cardCategory: action.payload
      }
    case GO_TO_NEXT_CARD:
      return {
        ...state,
        cardCounter: state.cardCounter < 5 
          ? state.cardCounter += 1
          : state.cardCounter = 0
      }
    case GO_TO_PREVIOUS_CARD:
      return {
        ...state,
        cardCounter: state.cardCounter > 0
          ? state.cardCounter -= 1
          : state.cardCounter = 5
      }
    default:
      return state;
  }
}